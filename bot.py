from cogs.util.bot import HTSTEMBote


def main():
    bot = HTSTEMBote()
    bot.run()


if __name__ == '__main__':
    main()
